create database Danielle;
use Danielle;

create table rol(
    id int primary key auto_increment,
    description varchar(50)
);

insert into rol values
(null, 'user'),
(null, 'admin'),
(null, 'supervisor');

create table user(
    id_user int primary key auto_increment,
    user varchar(50),
    fullname varchar(50),
    email varchar(50),
    password varchar(250),
    fk_rol int,
    foreign key(fk_rol) references rol(id)
);

create table producto(
    id_producto int primary key auto_increment,
    nombre varchar(250),
    imagen varchar(250),
    precio double,
    cantidad int
);