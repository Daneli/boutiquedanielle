const { Router } = require('express')
const router = Router()
const controller = require('../controllers/producto.controller')
const { isLoggedIn } =require('../lib/auth');

router.get('/producto', isLoggedIn, controller.Mostrar)
router.get('/blusas', isLoggedIn, controller.Blusas)
router.get('/faldas', isLoggedIn, controller.Faldas)
router.get('/short', isLoggedIn, controller.Short)
router.get('/top', isLoggedIn, controller.Top)
router.get('/trajes', isLoggedIn, controller.Trajes)
router.get('/vestido', isLoggedIn, controller.Vestido)
router.get('/leer', isLoggedIn, controller.Leer)
router.get('/lerrcarousel', isLoggedIn, controller.Leercarousel)

router.get('/producto/add', isLoggedIn, controller.RenderAdd)
router.post('/producto/add', isLoggedIn, controller.Insert)
router.get('/producto/edit/:id', isLoggedIn, controller.RenderEdit)
router.post('/producto/edit/:id', isLoggedIn, controller.Edit)
router.get('/producto/delete/:id', isLoggedIn, controller.Delete)

module.exports = router