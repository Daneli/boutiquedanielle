const pool = require("../lib/database")

class ProductoController {
    async Mostrar(req, res) {
        const producto = await pool.query('select * from producto')
        res.render("productos/producto", {producto})
    }
    async Blusas(req, res){
        res.render("productos/blusas")
    }
    async Faldas(req, res){
        res.render("productos/faldas")
    }
    async Short(req, res){
        res.render("productos/short")
    }
    async Top(req, res){
        res.render("productos/top")
    }
    async Trajes(req, res){
        res.render("productos/trajes")
    }
    async Vestido(req, res){
        res.render("productos/vestido")
    }
    async Leer(req, res){
        res.render("productos/leer")
    }
    async Leercarousel(req, res){
        res.render("productos/lerrcarousel")
    }

    //Insertar producto
    RenderAdd(req, res) {
        const { id } = req.params
        console.log(id)
        res.render('productos/add',{id})
    }

    async Insert(req, res) {
        try {
            const {nombre, imagen, precio, cantidad} = req.body

            const newProducto = {
                nombre,
                imagen,
                precio,
                cantidad,
            }

            await pool.query('insert into producto set ?', [newProducto])
            res.redirect('/producto')
        } catch (error) {
            res.redirect('/producto/add')
        }
    }

    //Editar producto
    async RenderEdit(req, res) {
        const { id } = req.params
        const producto = await pool.query('select * from producto where id_producto = ?', [id])
        try {
            if (producto[0].id_producto == id) {
                res.render('productos/edit', { producto: producto[0] })
            }
        } catch (error) {
            res.redirect('/producto')
        }
    }
    async Edit(req, res){
        const { id } = req.params
        try {
            const {nombre, imagen, precio, cantidad} = req.body

            const newProducto = {
                nombre,
                imagen,
                precio,
                cantidad,
            }

            await pool.query('update producto set ? where id_producto = ?', [newProducto, id])
            res.redirect('/producto')
        } catch (error) {
            res.redirect('/producto/edit/' + id)
        }
    }

    //Eliminar producto
    async Delete(req, res) {
        const { id } = req.params
        const productos = await pool.query('select * from producto where id_producto = ?', [id])

        try {
            if (id == productos[0].id_producto) {
                await pool.query('delete from producto where id_producto = ?', [id])
                res.redirect('/producto')
            }
        } catch (error) {
            res.redirect('/producto')
        }
    }
}

module.exports = new ProductoController();